import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MessageserviceProvider } from '../../providers/messageservice/messageservice';

import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-usuario',
  templateUrl: 'usuario.html',
})
export class UsuarioPage {
  public codigo:any;
  public nome:any;
  public usuario:any;
  public senha:any;
  public email:any;
  public telefone:any;
  public mensagem: any;

  public beer: any={};
  
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public alerta: AlertController,private servico:MessageserviceProvider) {
  }

  Salvar(){
    this.servico.postUsuario(this.nome,this.usuario,this.senha,this.email,this.telefone)
    .map(res=> res.json())
    .subscribe(data=> {
      this.beer = data;
      console.log(data.Mensagem);
    });
    
    console.log("Salvo com sucesso. "+this.beer.nome);
    console.log("Mensagem: " + this.beer.Mensagem);
    this.AlertaMsg();
  }

  AlertaMsg() {
    const alert = this.alerta.create({
      title: 'Informação !',
      subTitle: this.beer.Mensagem,
      buttons: ['Fechar']
    });
    alert.present();
  }
  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad UsuarioPage');
  // }

}
