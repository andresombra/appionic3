import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  public nome : any;
  public data: any;
  public frmpag: any;
  constructor(public navCtrl: NavController, public prm: NavParams) {
    console.log("Recebeu:"+this.prm.data.nome);
    this.nome = this.prm.data.nome;
    this.data = this.prm.data.data;
    this.frmpag = this.prm.data.frmpag;
  }

}
