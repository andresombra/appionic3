import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Http, Headers } from "@angular/http";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class MessageserviceProvider {

  private url: string = "http://andresombra.net/apidocs/";
  private headers = new Headers({'Accept':'application/json'});
  
  //public beer:any={};
  constructor(private http: Http) {
    console.log('Hello MessageserviceProvider Provider');
  }

  getUsuario(id){
    let acao = 'usuarios/getcodigo/' + id;
    console.log(acao);
    console.log(this.url+acao);
    //return this.http.post(this.url + acao, { headers: this.headers })
    return this.http.get(this.url+acao)
           .do(res=> res.json());
  }

  postUsuario(nome,usuario,senha,email,telefone){
    console.log(nome);
    console.log(usuario);
    let postData = new FormData();
    postData.append("nome",nome);
    postData.append("usuario", usuario);
    //,{nome:nome,usuario:usuario,senha:senha,email,telefone})
    //this.data = this.http.post(this.url+'usuarios/incluir', postData);
    //this.data.subscribe(res => {
    return this.http.post(this.url+'usuarios/incluir', postData)
      .do(res => res.json());
      //{
      //console.log(res.json());
      //});
    
  }
}
