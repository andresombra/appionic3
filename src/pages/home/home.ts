import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { _ngDevMode } from '@angular/core/src/render3/ng_dev_mode';
import { AboutPage } from '../about/about';
import { UsuarioPage } from '../usuario/usuario';
import { MessageserviceProvider } from '../../providers/messageservice/messageservice';

import { IUsuario } from '../../interfaces/IUsuario'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  public nome:any;
  public data: any;
  public frmpag: any;
  public usuario : any;

  constructor(public navCtrl: NavController, public prm: NavParams, private MessageserviceProvider: MessageserviceProvider) {
    //console.log(this.nome);
    this.getMsg();
  }

  getMsg(){
    //this.usuario = this.MessageserviceProvider.getUsuario(1).subscribe(data=> console.log(data.json()
    this.MessageserviceProvider.getUsuario(1)
    .map(res=>res.json())
    .subscribe(data => {
      console.log(data);
      console.log('Any:' + data.Codigo);
      console.log('Teste:' + data.Codigo);
      this.nome = data.Usuario.USU_NOME;  
      this.frmpag = data.Codigo;
    });
    
  }
  Salvar(){
   //console.log(this.prm.get('nome'));
    //console.log(this.nome);
   this.navCtrl.setRoot(AboutPage,this);
   // let _nome = this.prm.get('nome');
   //this.navCtrl.setRoot("AboutPage",{});
   // nome: this.prm.get("nome")  
    //let _nome = this.prm.get('nome');
    //console.log(_nome)
  // }
  }

  

}
